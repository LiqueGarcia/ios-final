//
//  LoginViewController.swift
//  Proy_Final-DAMII
//
//  Created by Andres Leon on 9/06/18.
//  Copyright © 2018 Andres Leon. All rights reserved.
//

import UIKit

extension UITextField{
    
    func setPadding(){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.frame.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
}
class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var txtUsuario: UITextField!
    @IBOutlet weak var txtContrasena: UITextField!
    @IBOutlet weak var btnIngresar: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtUsuario.setPadding()
        txtContrasena.setPadding()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}


//
//  LocalViewController.swift
//  Proy_Final-DAMII
//
//  Created by Andres Leon on 19/06/18.
//  Copyright © 2018 Andres Leon. All rights reserved.
//

import UIKit
import MapKit

final class LocalAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    var title: String?
    var subtitle: String?
    
    init(coordinate: CLLocationCoordinate2D, title: String?, subtitle: String?){
        self.coordinate = coordinate
        self.title = title
        self.subtitle = subtitle
        
        super.init()
    }
    
    var region: MKCoordinateRegion{
        let span = MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5)
        return MKCoordinateRegion(center: coordinate, span: span)
    }
}
class LocalViewController: UIViewController {


    @IBOutlet weak var mapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()

    
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        mapView.register(MKMarkerAnnotationView.self,
                         forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
        
        let mitCoordinate = CLLocationCoordinate2D(latitude: 42.3601, longitude: -71.0942)
        let mitAnnotation = LocalAnnotation(coordinate: mitCoordinate, title: "Estación Ayacucho", subtitle: "Ayacucho es una estación de la Línea 1 del Metro de Lima en Perú. Está ubicada en la intersección de las avenidas Tomás Marsano y Ayacucho en el distrito de Surco. La estación es elevada y su entorno inmediato es residencial y comercial.")
        
        mapView.addAnnotation(mitAnnotation)
        mapView.setRegion(mitAnnotation.region, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LocalViewController: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let localAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier) as? MKMarkerAnnotationView{
            
            localAnnotationView.animatesWhenAdded = true
            localAnnotationView.titleVisibility = .adaptive
            localAnnotationView.subtitleVisibility = .adaptive
            
            return localAnnotationView
        }
        return nil
    }
}

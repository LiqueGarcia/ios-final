//
//  BicicletaViewController.swift
//  Proy_Final-DAMII
//
//  Created by Andres Leon on 18/06/18.
//  Copyright © 2018 Andres Leon. All rights reserved.
//

import UIKit

class BicicletaViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
extension BicicletaViewController: UITableViewDataSource, UITableViewDelegate{
    //numberOfRowsInSection
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = tableView.dequeueReusableCell(withIdentifier: "ItemBicicletaTableViewCell", for: indexPath) as! ItemBicicletaTableViewCell
        
        return item
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ReservaViewController") as! ReservaViewController
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

//
//  RegisterViewController.swift
//  Proy_Final-DAMII
//
//  Created by Andres Leon on 17/06/18.
//  Copyright © 2018 Andres Leon. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var txtNombres: UITextField!
    @IBOutlet weak var txtApellidos: UITextField!
    @IBOutlet weak var txtTipDocumento: UITextField!
    @IBOutlet weak var txtNumDocumento: UITextField!
    @IBOutlet weak var txtTelefono: UITextField!
    @IBOutlet weak var txtContrasena02: UITextField!
    @IBOutlet weak var txtContrasena01: UITextField!
    @IBOutlet weak var txtCorreo: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        txtNombres.setPadding()
        txtApellidos.setPadding()
        txtTipDocumento.setPadding()
        txtNumDocumento.setPadding()
        txtTelefono.setPadding()
        txtCorreo.setPadding()
        txtContrasena01.setPadding()
        txtContrasena02.setPadding()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

